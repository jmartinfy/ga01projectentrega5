package es.unex.giiis.asee.ccopas;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.ccopas.uis.BarManagerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AboutFragmentTest {

    @Rule
    public ActivityTestRule<BarManagerActivity> mActivityTestRule = new ActivityTestRule<>(BarManagerActivity.class);

    @Test
    public void aboutFragmentTest() throws InterruptedException {
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Abriendo menú"),
                        childAtPosition(
                                allOf(withId(R.id.bar_manager_toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction navigationMenuItemView = onView(
                allOf(withId(R.id.nav_about),
                        childAtPosition(
                                allOf(withId(R.id.design_navigation_view),
                                        childAtPosition(
                                                withId(R.id.navigation_view),
                                                0)),
                                5),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.acercaDeTV), withText("Autores"),
                        withParent(withParent(withId(R.id.acerca_de_drawer))),
                        isDisplayed()));
        textView.check(matches(withText("Autores")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.acercaDeTV_JuanFran), withText("Juan Francisco Martín"),
                        withParent(withParent(withId(R.id.acerca_de_drawer))),
                        isDisplayed()));
        textView2.check(matches(withText("Juan Francisco Martín")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.acercaDeTV_sergio), withText("Sergio Barrantes"),
                        withParent(withParent(withId(R.id.acerca_de_drawer))),
                        isDisplayed()));
        textView3.check(matches(withText("Sergio Barrantes")));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.acercaDeTV_david), withText("David Merino"),
                        withParent(withParent(withId(R.id.acerca_de_drawer))),
                        isDisplayed()));
        textView4.check(matches(withText("David Merino")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
