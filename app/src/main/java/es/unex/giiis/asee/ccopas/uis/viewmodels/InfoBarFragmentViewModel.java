package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;

public class InfoBarFragmentViewModel extends ViewModel {
    private final BarRepository barRepository;

    public InfoBarFragmentViewModel(BarRepository barRepository){
        this.barRepository = barRepository;
    }
    public void actualizarBar(Bar bar){
        barRepository.actualizarBar(bar);
    }
}
