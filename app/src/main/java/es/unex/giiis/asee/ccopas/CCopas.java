package es.unex.giiis.asee.ccopas;

import android.app.Application;

import es.unex.giiis.asee.ccopas.AppContainer;

public class CCopas extends Application {
    public AppContainer appContainer;

    @Override
    public void onCreate(){
        super.onCreate();
        appContainer = new AppContainer(this);
    }
}
