package es.unex.giiis.asee.ccopas.API;

import java.io.IOException;

import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.model.cocktailAPI.CocktailDB;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BebidaNetworkLoaderRunnable implements Runnable{
    private final OnBebidaLoadedListener mOnBebidaLoadedListener;

    public BebidaNetworkLoaderRunnable(OnBebidaLoadedListener onReposLoadedListener){
        mOnBebidaLoadedListener = onReposLoadedListener;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.thecocktaildb.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CocktailDBDataService service = retrofit.create(CocktailDBDataService.class);

        try {
            CocktailDB drinkDB = service.listBebida().execute().body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnBebidaLoadedListener.onBebidaLoaded(drinkDB));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
