package es.unex.giiis.asee.ccopas.model;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.unex.giiis.asee.ccopas.roomdb.Converter.DateConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.TipoReviewConverter;

@Entity(tableName = "reviews")
public class Review implements Serializable {
    @PrimaryKey (autoGenerate = true)
    private long reviewId;
    @ColumnInfo
    private String nombreBar;

    @TypeConverters(TipoReviewConverter.class)
    private TipoReview tipo;

    public String getNombreBar() {
        return nombreBar;
    }

    public void setNombreBar(String nombreBar) {
        this.nombreBar = nombreBar;
    }

    @ColumnInfo
    private String user;
    @ColumnInfo
    private String title;
    @ColumnInfo
    private String text;
    @TypeConverters(DateConverter.class)
    private Date date;
    @ColumnInfo
    private float rate;
    @Ignore
    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd", Locale.US);
    public Review(long reviewId,String nombreBar, String user, String title, String text, Date date, float rate) {
        this.reviewId = reviewId;
        this.nombreBar=nombreBar;
        this.user = user;
        this.title = title;
        this.text = text;
        this.date = date;
        this.rate = rate;
    }
    @Ignore
    public final static String ID = "reviewId";
    @Ignore
    public final static String USER = "user";
    @Ignore
    public final static String TITLE = "title";
    @Ignore
    public final static String TEXT = "text";
    @Ignore
    public final static String DATE = "date";
    @Ignore
    public final static String RATING = "rating";

    @Ignore
    public Review() {

    }
    public long getReviewId() {
        return reviewId;
    }

    public void setReviewId(long reviewId) {
        this.reviewId = reviewId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public static void packageIntent(Intent intent, String author,String title, String text, float rating , String date){
        intent.putExtra(Review.USER,author);
        intent.putExtra(Review.TITLE,title);
        intent.putExtra(Review.TEXT,text);
        intent.putExtra(Review.DATE,date);
        intent.putExtra(Review.RATING,rating);
    }

    public TipoReview getTipo() {
        return tipo;
    }

    public void setTipo(TipoReview tipo) {
        this.tipo = tipo;
    }

    @Ignore
    public Review(Intent intent,String nombreBar){
        this.nombreBar = nombreBar;
        user = intent.getStringExtra(Review.USER);
        title = intent.getStringExtra(Review.TITLE);
        text = intent.getStringExtra(Review.TEXT);
        rate = intent.getFloatExtra(Review.RATING,-1);
        try {
            date=Review.FORMAT.parse(intent.getStringExtra(Review.DATE));
        } catch (ParseException e){
            date = new Date();
        }
    }
    public enum TipoReview {
        Bar,Bebida;
    }

}
