package es.unex.giiis.asee.ccopas;

import android.content.Context;

import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarManagerViewModelFactory;

public class AppContainer {
    private BarDatabase database;
    private BarNetworkDataSource barNetworkDataSource;
    private BebidaNetworkDataSource bebidaNetworkDataSource;
    public BarRepository barRepository;
    public BarManagerViewModelFactory factory;

    public AppContainer(Context context){
        database = BarDatabase.getInstance(context);
        barNetworkDataSource = BarNetworkDataSource.getInstance();
        bebidaNetworkDataSource = BebidaNetworkDataSource.getInstance();
        barRepository = BarRepository.getInstance(database.getDao(),barNetworkDataSource,database.getBebidaDao(),bebidaNetworkDataSource,database.getReviewDao());
        factory= new BarManagerViewModelFactory(barRepository);
    }
}
