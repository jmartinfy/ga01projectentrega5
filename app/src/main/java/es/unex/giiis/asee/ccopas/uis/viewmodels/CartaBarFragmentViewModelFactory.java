package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.ccopas.BarRepository;

public class CartaBarFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final BarRepository barRepository;

    public CartaBarFragmentViewModelFactory(BarRepository barRepository){
        this.barRepository=barRepository;
    }

    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CartaBarFragmentViewModel(barRepository);
    }
}
