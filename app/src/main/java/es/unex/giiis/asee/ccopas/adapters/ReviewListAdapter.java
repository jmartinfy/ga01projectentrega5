package es.unex.giiis.asee.ccopas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Review;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ViewHolder> {
    private List<Review> mItems = new ArrayList<>();
    Context mContext;

    private final OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(Review review);
    }

    public ReviewListAdapter(Context context, OnItemClickListener listener){
        mContext= context;
        this.listener=listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from((parent.getContext())).inflate(R.layout.review_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(mContext,view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mItems.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
    public void add(Review review){
        mItems.add(review);
        notifyDataSetChanged();
    }
    public void addAll(List reviewList){
        mItems.clear();
        mItems.addAll(reviewList);
        notifyDataSetChanged();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private static final int STARS_NUMBER = 5;
        TextView userReview;
        TextView titleReview;
        TextView textReview;
        RatingBar ratingBar;
        TextView dateReview;




        public ViewHolder(Context context, @NonNull View itemView) {
            super(itemView);
            userReview = itemView.findViewById(R.id.user_review);
            titleReview = itemView.findViewById(R.id.title_review);
            textReview = itemView.findViewById(R.id.text_review);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            dateReview= itemView.findViewById(R.id.date_review);
        }

        public void bind(final Review review, final OnItemClickListener listener){

            //Display the review author's name
            userReview.setText(review.getUser());

            //Display the title of the review
            titleReview.setText(review.getTitle());

            //Display the review's body
            textReview.setText(review.getText());

            //Display the review's date
            dateReview.setText(Review.FORMAT.format(review.getDate()));
            //Display the rating
            ratingBar.setNumStars(STARS_NUMBER);
            ratingBar.setRating(review.getRate());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(review);
                }
            });
        }
    }
}
