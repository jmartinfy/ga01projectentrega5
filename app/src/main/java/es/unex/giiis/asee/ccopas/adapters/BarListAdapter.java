package es.unex.giiis.asee.ccopas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.R;

public class BarListAdapter extends RecyclerView.Adapter<BarListAdapter.ImageViewHolder> {
    private List<Bar> mItems = new ArrayList<>();
    Context mContext;

    private final OnItemClickListener listener;

    public void setFilters(ArrayList<Bar> listaBares){
        this.mItems.clear();
        this.mItems.addAll(listaBares);
        notifyDataSetChanged();
    }

    public interface OnItemClickListener{
        void onItemClick(Bar bar);
    }

    public BarListAdapter(Context context, OnItemClickListener listener){
        mContext= context;
        this.listener=listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from((parent.getContext())).inflate(R.layout.bar_item,parent,false);
        ImageViewHolder imageViewHolder = new ImageViewHolder(mContext,view);

        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.bind(mItems.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void add(Bar bar){
        mItems.add(bar);
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<Bar> bares){

        mItems.clear();
        mItems.addAll(bares);
        notifyDataSetChanged();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView fotoBar;
        TextView nombreBar;
        TextView direccionBar;

        public ImageViewHolder(Context context,@NonNull View itemView) {
            super(itemView);
            nombreBar = itemView.findViewById(R.id.nombreBar);
            fotoBar = itemView.findViewById(R.id.fotoBar);
            direccionBar = itemView.findViewById(R.id.direccionBar);
        }

        public void bind(final Bar bar, final OnItemClickListener listener){
            //Display the foto in ImageView
            fotoBar.setImageResource(bar.getMain_photo());

            //Display the name of the bar
            nombreBar.setText(bar.getNombre());

            //Display the location of the bar
            direccionBar.setText(bar.getDireccion());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(bar);
                }
            });
        }
    }


}
