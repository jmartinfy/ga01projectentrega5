package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.model.Bar;

public class FavBarListFragmentViewModel extends ViewModel {
    private final BarRepository barRepository;
    private final LiveData<List<Bar>> mBares;

    public FavBarListFragmentViewModel(BarRepository barRepository){
        this.barRepository = barRepository;
        mBares = barRepository.obtenerBaresFavoritosActualizados();
    }

    public LiveData<List<Bar>> getBares(){
        return mBares;
    }
}
