
package es.unex.giiis.asee.ccopas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import es.unex.giiis.asee.ccopas.model.attr.*;

public class Binding {

    @SerializedName("uri")
    @Expose
    private Uri uri;
    @SerializedName("schema_url")
    @Expose
    private SchemaUrl schemaUrl;
    @SerializedName("schema_email")
    @Expose
    private SchemaEmail schemaEmail;
    @SerializedName("geo_long")
    @Expose
    private GeoLong geoLong;
    @SerializedName("schema_telephone")
    @Expose
    private SchemaTelephone schemaTelephone;
    @SerializedName("geo_lat")
    @Expose
    private GeoLat geoLat;
    @SerializedName("om_capacidadPersonas")
    @Expose
    private OmCapacidadPersonas omCapacidadPersonas;
    @SerializedName("rdfs_label")
    @Expose
    private RdfsLabel rdfsLabel;
    @SerializedName("om_sirveComida")
    @Expose
    private OmSirveComida omSirveComida;
    @SerializedName("schema_address_postalCode")
    @Expose
    private SchemaAddressPostalCode schemaAddressPostalCode;
    @SerializedName("schema_address_addressCountry")
    @Expose
    private SchemaAddressAddressCountry schemaAddressAddressCountry;
    @SerializedName("schema_address_addressLocality")
    @Expose
    private SchemaAddressAddressLocality schemaAddressAddressLocality;
    @SerializedName("schema_address_streetAddress")
    @Expose
    private SchemaAddressStreetAddress schemaAddressStreetAddress;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public SchemaUrl getSchemaUrl() {
        return schemaUrl;
    }

    public void setSchemaUrl(SchemaUrl schemaUrl) {
        this.schemaUrl = schemaUrl;
    }

    public SchemaEmail getSchemaEmail() {
        return schemaEmail;
    }

    public void setSchemaEmail(SchemaEmail schemaEmail) {
        this.schemaEmail = schemaEmail;
    }

    public GeoLong getGeoLong() {
        return geoLong;
    }

    public void setGeoLong(GeoLong geoLong) {
        this.geoLong = geoLong;
    }

    public SchemaTelephone getSchemaTelephone() {
        return schemaTelephone;
    }

    public void setSchemaTelephone(SchemaTelephone schemaTelephone) {
        this.schemaTelephone = schemaTelephone;
    }

    public GeoLat getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(GeoLat geoLat) {
        this.geoLat = geoLat;
    }

    public OmCapacidadPersonas getOmCapacidadPersonas() {
        return omCapacidadPersonas;
    }

    public void setOmCapacidadPersonas(OmCapacidadPersonas omCapacidadPersonas) {
        this.omCapacidadPersonas = omCapacidadPersonas;
    }

    public RdfsLabel getRdfsLabel() {
        return rdfsLabel;
    }

    public void setRdfsLabel(RdfsLabel rdfsLabel) {
        this.rdfsLabel = rdfsLabel;
    }

    public OmSirveComida getOmSirveComida() {
        return omSirveComida;
    }

    public void setOmSirveComida(OmSirveComida omSirveComida) {
        this.omSirveComida = omSirveComida;
    }

    public SchemaAddressPostalCode getSchemaAddressPostalCode() {
        return schemaAddressPostalCode;
    }

    public void setSchemaAddressPostalCode(SchemaAddressPostalCode schemaAddressPostalCode) {
        this.schemaAddressPostalCode = schemaAddressPostalCode;
    }

    public SchemaAddressAddressCountry getSchemaAddressAddressCountry() {
        return schemaAddressAddressCountry;
    }

    public void setSchemaAddressAddressCountry(SchemaAddressAddressCountry schemaAddressAddressCountry) {
        this.schemaAddressAddressCountry = schemaAddressAddressCountry;
    }

    public SchemaAddressAddressLocality getSchemaAddressAddressLocality() {
        return schemaAddressAddressLocality;
    }

    public void setSchemaAddressAddressLocality(SchemaAddressAddressLocality schemaAddressAddressLocality) {
        this.schemaAddressAddressLocality = schemaAddressAddressLocality;
    }

    public SchemaAddressStreetAddress getSchemaAddressStreetAddress() {
        return schemaAddressStreetAddress;
    }

    public void setSchemaAddressStreetAddress(SchemaAddressStreetAddress schemaAddressStreetAddress) {
        this.schemaAddressStreetAddress = schemaAddressStreetAddress;
    }

}
