package es.unex.giiis.asee.ccopas.API;

import es.unex.giiis.asee.ccopas.model.cocktailAPI.CocktailDB;

public interface OnBebidaLoadedListener {
    public void onBebidaLoaded(CocktailDB cocktailDB);
}
