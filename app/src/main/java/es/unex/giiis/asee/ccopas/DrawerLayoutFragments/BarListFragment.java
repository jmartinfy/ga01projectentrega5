package es.unex.giiis.asee.ccopas.DrawerLayoutFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.AppContainer;
import es.unex.giiis.asee.ccopas.CCopas;
import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.uis.AddBarActivity;
import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.uis.DetalleBarActivity;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.BarListAdapter;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModelFactory;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BarListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BarListFragment extends Fragment implements SearchView.OnQueryTextListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "title";
    private static final int ADD_TODO_ITEM_REQUEST = 0;

    private RecyclerView barListRecyclerView;
    private BarListAdapter barListAdapter;
    private ArrayList<Bebida> bebidas;
    private BarListFragmentViewModel mViewModel;

    public ArrayList<Bar> getBares() {
        return bares;
    }

    private ArrayList<Bar> bares = new ArrayList<>();
    private SearchView svBuscar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BarListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title Parameter 1.
     * @return A new instance of fragment BarListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BarListFragment newInstance(String title) {
        BarListFragment fragment = new BarListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        fragment.setArguments(args);
        return fragment;
    }


    private void initSearchView(View v) {
        svBuscar=v.findViewById(R.id.svBuscarBar);
        svBuscar.setQueryHint("Buscar bar");
        svBuscar.setOnQueryTextListener(this);
        svBuscar.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                barListAdapter.setFilters(bares);
                return false;
            }
        });
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v= inflater.inflate(R.layout.content_bar_manager, container, false);
        initSearchView(v);


        BarListFragmentViewModelFactory factory = InjectorUtils.provideBarListFragmentViewModelFactory(this.getActivity().getApplicationContext());
        mViewModel = new ViewModelProvider(this,factory).get(BarListFragmentViewModel.class);


        FloatingActionButton add_bar =  v.findViewById(R.id.add_bar_fab);
        add_bar.setVisibility(View.VISIBLE);
        add_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getApplicationContext();
                Intent addReviewIntent = new Intent(view.getContext(), AddBarActivity.class);
                startActivityForResult(addReviewIntent,ADD_TODO_ITEM_REQUEST);
            }
        });



        barListRecyclerView = v.findViewById(R.id.bar_reclycler_view);
        barListRecyclerView.setHasFixedSize(true);
        barListRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        barListAdapter=new BarListAdapter(v.getContext(), new BarListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Bar bar) {
                Context context = v.getContext().getApplicationContext();
                CharSequence text = "bar " + bar.getNombre() + " pulsado";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                Intent irBar = new Intent(context, DetalleBarActivity.class);
                irBar.putExtra("InstanciaBar", bar);
                irBar.putExtra("Bar",bar.getNombre());
                startActivity(irBar);

            }
        });
        barListRecyclerView.setAdapter(barListAdapter);

        mViewModel.getBares().observe(getViewLifecycleOwner(), bares -> onBarLoaded((ArrayList<Bar>) bares));
        //TODO BEbidas
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_TODO_ITEM_REQUEST){
            if(resultCode==RESULT_OK){
                assert data != null;
                Bar newBar= new Bar(data);

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Bebida> Listbebidas;
                        Listbebidas = bebidas;
                        //newBar.setBebidasBar(Listbebidas);
                        mViewModel.insertarBar(newBar);
                    }
                });
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ArrayList<Bar> barBuscado=new ArrayList<>();
        if(newText.length() > 0) {
            svBuscar.setQueryHint("");

            barBuscado = this.filter(newText);
            if (barBuscado.size() > 0)
                barListAdapter.setFilters(barBuscado);
        }else {
            barListAdapter.setFilters((ArrayList<Bar>) bares);
        }
        return true;
    }

    public ArrayList<Bar> filter(String nombreBuscar){
        ArrayList<Bar> baresEncontrados= new ArrayList<>();
        String nombreBar;
        nombreBuscar=nombreBuscar.toLowerCase();

        for (Bar bar: bares){
            nombreBar=bar.getNombre().toLowerCase();
            if(nombreBar.contains(nombreBuscar)){
                baresEncontrados.add(bar);
            }
        }
        return baresEncontrados;
    }



    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        svBuscar.setVisibility(View.VISIBLE);
    }
    public void onBarLoaded(ArrayList<Bar> bares){
        getActivity().runOnUiThread(() -> barListAdapter.addAll(bares));
        getActivity().runOnUiThread(() -> this.bares=bares);
    }
    private void onBebidaLoaded(ArrayList<Bebida> bebidas) {
        getActivity().runOnUiThread(() -> this.bebidas = bebidas);
    }


}
