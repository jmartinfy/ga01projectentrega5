package es.unex.giiis.asee.ccopas.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.roomdb.Converter.ReviewListConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.TipoBebidaConverter;


//room
@Entity(tableName = "bebidas")
public class Bebida implements Serializable {
    public TipoBebida getType() {
        return type;
    }

    @PrimaryKey @NonNull
    private String nombre;
    @ColumnInfo
    private String URL;

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public boolean isFav() {
        return fav;
    }

    @ColumnInfo
    private boolean fav;

    @TypeConverters(TipoBebidaConverter.class)
    private TipoBebida type = TipoBebida.Alcohol;

    public List<Review> getBebidaReviews() {
        return bebidaReviews;
    }

    public void setBebidaReviews(List<Review> bebidaReviews) {
        this.bebidaReviews = bebidaReviews;
    }

    @TypeConverters(ReviewListConverter.class)
    private List<Review> bebidaReviews = new ArrayList<>();
    @Ignore
    public Bebida(long bebidaId, String nombre, TipoBebida type) {

        this.nombre = nombre;
        this.type=type;
    }

    public Bebida(String nombre, String URL, TipoBebida type, List<Review> bebidaReviews) {
        this.nombre = nombre;
        this.URL = URL;
        this.type = type;
        this.bebidaReviews = bebidaReviews;
    }

    @Ignore
    public Bebida(String name,String URL) {
        this.setNombre(name);
        this.setURL(URL);
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public void setType(TipoBebida type) {
        this.type = type;
    }

    public String getTypeDrink(){
        return this.type.name();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public enum TipoBebida {
        Alcohol,Refresco,Espiritosa;
    }

}