package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.ccopas.BarRepository;

public class ReviewFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final BarRepository barRepository;
    private String nombre;

    public ReviewFragmentViewModelFactory(BarRepository barRepository,String nombre){
        this.barRepository=barRepository;
        this.nombre=nombre;
    }

    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ReviewFragmentViewModel(barRepository,nombre);
    }
}
