package es.unex.giiis.asee.ccopas.roomdb.Converter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bebida;

public class BebidasListConverter {
    @TypeConverter
    public List<Bebida> toReviewList(String bebidaListStr){
        if(bebidaListStr==null){
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Bebida>>(){
        }.getType();
        List<Bebida> bebidaList = gson.fromJson(bebidaListStr,type);
        return bebidaList;
    }
    @TypeConverter
    public String fromReviewList (List<Bebida> bebidaList){
        if (bebidaList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Bebida>>() {
        }.getType();
        String json = gson.toJson(bebidaList, type);
        return json;
    }
}
