package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.ccopas.BarRepository;

public class BarManagerActivityViewModel extends ViewModel {
    private final BarRepository barRepository;

    public BarManagerActivityViewModel(BarRepository barRepository){
        this.barRepository = barRepository;
    }
}
