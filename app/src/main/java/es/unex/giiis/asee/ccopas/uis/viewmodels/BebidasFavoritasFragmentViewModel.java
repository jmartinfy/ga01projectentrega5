package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.model.Bebida;

public class BebidasFavoritasFragmentViewModel extends ViewModel {
    private final BarRepository barRepository;
    private final LiveData<List<Bebida>> mBebidas;

    public BebidasFavoritasFragmentViewModel(BarRepository barRepository){
        this.barRepository = barRepository;
        mBebidas = barRepository.obtenerBebidasFavoritasActualizadas();
    }
    public LiveData<List<Bebida>> getmBebidas() { return mBebidas;}
}
