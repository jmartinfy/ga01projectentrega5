package es.unex.giiis.asee.ccopas;

import androidx.fragment.app.Fragment;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.BarListFragment;
import es.unex.giiis.asee.ccopas.model.Bar;

public class CU07UnitTest {

    BarListFragment BLF = new BarListFragment();
    ArrayList<Bar> BaresEncontrados = new ArrayList<>();

    @Test
    public void testBuscarBares(){

        Bar mockBar = new Bar("abc1", "111222333", "direccionF", 1, false, "20");
        Bar mockBar2 = new Bar("def2", "111222333", "direccionF", 1, false, "20");
        Bar mockBar3 = new Bar("ghj3", "111222333", "direccionF", 1, false, "20");
        Bar mockBar4 = new Bar("j3", "111222333", "direccionF", 1, false, "20");

        BLF.getBares().add(mockBar);
        BLF.getBares().add(mockBar2);
        BLF.getBares().add(mockBar3);
        BLF.getBares().add(mockBar4);

        BaresEncontrados=BLF.filter("a");
        Assert.assertEquals(BaresEncontrados.size(),1);
        BaresEncontrados=BLF.filter("3");
        Assert.assertEquals(BaresEncontrados.size(),2);
        BaresEncontrados=BLF.filter("2");
        Assert.assertEquals(BaresEncontrados.size(),1);

    }

}
