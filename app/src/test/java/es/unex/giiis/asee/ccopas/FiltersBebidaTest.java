package es.unex.giiis.asee.ccopas;

import org.junit.Test;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.ui.tabsDetalleBar.CartaBarFragment;

import static org.junit.Assert.assertEquals;

public class FiltersBebidaTest {
    private CartaBarFragment fragment;
    @Test
    public void shouldFilterDrinks(){
        ArrayList<Bar> bares = new ArrayList<>();
        ArrayList<Review> reviews = new ArrayList<>();
        ArrayList<Bebida> bebidas = new ArrayList<>();
        Review review = new Review();
        review.setReviewId(1);
        review.setTitle("Autor");
        review.setUser("Autor");
        review.setText("Autor");
        review.setRate(5);
        reviews.add(review);
        Bar bar = new Bar("Hola","121212122","Direccion",R.drawable.bar_icon,false,"64");
        Bebida bebida = new Bebida("Nombre","URL", Bebida.TipoBebida.Alcohol,reviews);
        bebidas.add(bebida);
        bar.setBebidasBar(bebidas);
        fragment = new CartaBarFragment();
        fragment.getListBebidas().add(bebida);
        fragment.filter("Nombre",bebidas);
        assertEquals(fragment.getListBebidas().size(),1);

    }
}
